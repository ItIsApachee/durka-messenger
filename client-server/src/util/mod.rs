use tokio_util::codec::{
    length_delimited::{
        LengthDelimitedCodec,
    },
    Framed,
    FramedRead,
    FramedWrite,
};
use tokio::io::{
    AsyncWrite,
    AsyncRead,
};

/// FramedInterface for communication between server and client.
pub type FramedInterface<T> = Framed<T, LengthDelimitedCodec>;

pub type FramedInterfaceError = std::io::Error;

pub type Bytes = bytes::Bytes;
pub type BytesMut = bytes::BytesMut;

/// Creating FramedInterface for client and server.
pub fn create_framed_interface<T>(inner: T) -> Framed<T, LengthDelimitedCodec>
where T: AsyncWrite + AsyncRead {
    let codec = LengthDelimitedCodec::new();
    Framed::new(inner, codec)
}

// pub fn create_framed_rw<T, A, B>(inner: T) -> Framed<T, LengthDelimitedCodec>
// where T: AsyncWrite + AsyncRead {
//     let codec = LengthDelimitedCodec::new();
//     Framed::new(inner, codec)
// }