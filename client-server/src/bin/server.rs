use durka_messenger::Server;

use std::error::Error;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let server = Server::new();
    server.run("0.0.0.0:1337").await?;

    Ok(())
}