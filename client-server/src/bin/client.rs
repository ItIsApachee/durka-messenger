use durka_messenger::{
    Client,
    util::Bytes,
};

use tokio::io::{
    stdin,
    stdout,
};
use tokio_util::codec::{
    FramedRead,
    FramedWrite,
    LinesCodec,
};
use futures::{
    Sink,
    Stream,
    SinkExt,
    StreamExt,
    future::select_all,
};
use std::error::Error;
use std::str::from_utf8;


#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let client = Client::connect("127.0.0.1:1337").await?;
    let (mut w, mut r) = client.split();
    let mut handles = vec![];
    handles.push(tokio::spawn(async move {
        let mut lines = FramedWrite::new(stdout(), LinesCodec::new());
        while let Some(message) = r.next().await {
            match message {
                Ok(bytes) => {
                    let _ = lines.send(format!("New message: {:?}", from_utf8(&bytes))).await;
                },
                Err(e) => {
                    let _ = lines.send(format!("Connection error occured: {}", e)).await;
                    break;
                }
            }
        }
        Some::<()>(())
    }));
    handles.push(tokio::spawn(async move {
        let mut lines = FramedRead::new(stdin(), LinesCodec::new());
        let mut lines_out = FramedWrite::new(stdout(), LinesCodec::new());
        while let Some(message) = lines.next().await {
            match message {
                Ok(user_input) => {
                    let _ = w.send(Bytes::from(user_input)).await;
                }
                Err(e) => {
                    let _ = lines_out.send(format!("User input error occured: {}", e)).await;
                    break;
                }
            }
        }
        Some::<()>(())
    }));

    let _ = select_all(handles).await;

    Ok(())
}