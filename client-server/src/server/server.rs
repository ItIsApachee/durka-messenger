use std::{
    error::Error,
    net::SocketAddr,
    sync::Arc,
    collections::HashMap,
    io,
    pin::Pin,
    task::{
        Poll, Context,
    }
};
use tokio::{
    sync::{
        Mutex,
        mpsc,
        mpsc::UnboundedReceiver as Rx,
        mpsc::UnboundedSender as Tx,
    },
    net::{
        TcpListener, TcpStream,
    },
    stream::{
        Stream, StreamExt,
    },
};
use futures::SinkExt;

use super::super::util::{
    Bytes,
    FramedInterface,
    FramedInterfaceError,
    create_framed_interface,
};

pub struct Server {
    state: Arc<Mutex<SharedState>>
}

impl Server {
    pub fn new() -> Self {
        Server {
            state: Arc::new(Mutex::new(SharedState::new())),
        }
    }

    pub async fn run(&self, addr: &str) -> Result<(), Box<dyn Error>> {
        let mut listener = TcpListener::bind(addr).await?;
        println!("Listening on: {}", addr);

        loop {
            let (stream, sock_addr) = listener.accept().await?;

            let state = self.state.clone();
            tokio::spawn(async move {
                let _ = Self::handle(state, stream, sock_addr).await;
            });
        }
    }

    async fn handle(state: Arc<Mutex<SharedState>>, stream: TcpStream, sock_addr: SocketAddr) -> Result<(), Box<dyn Error>> {
        println!("[{}] Handling connection...", sock_addr);
        
        let lines = create_framed_interface(stream);
        let mut member = Member::new(state.clone(), lines).await?;

        while let Some(result) = member.next().await {
            match result {
                Ok(Message::Broadcast(msg)) => {
                    member.broadcast(std::str::from_utf8(&msg)?).await;
                },
                Ok(Message::Received(msg)) => {
                    member.send(msg).await?;
                },
                Err(e) => {
                    eprintln!("[{}] FramedInterfaceError occured: {}", sock_addr, e);
                    break;
                },
            }
        }
        member.drop().await;

        Ok(())
    }
}

struct SharedState {
    members: HashMap<SocketAddr, Tx<Bytes>>
}

impl SharedState {
    fn new() -> Self {
        SharedState {
            members: HashMap::new(),
        }
    }

    fn broadcast(&mut self, sender: SocketAddr, message: &Bytes) {
        for (reciver, tx) in self.members.iter() {
            if *reciver != sender {
                let _ = tx.send(message.clone());
            }
        }
    }
}

struct Member {
    lines: FramedInterface<TcpStream>,
    reciever: Rx<Bytes>,
    addr: SocketAddr,
    state: Arc<Mutex<SharedState>>,
}

impl Member {
    async fn new(
        state: Arc<Mutex<SharedState>>,
        lines: FramedInterface<TcpStream>
    ) -> io::Result<Member> {
        let addr = lines.get_ref().peer_addr()?;
        let (tx, rx) = mpsc::unbounded_channel();

        state.lock().await.members.insert(addr, tx);

        {
            let mut state = state.lock().await;
            let msg = format!("[SRV] [{}] connected", addr);
            println!("{}", msg);
            state.broadcast(addr, &Bytes::from(msg));
        }

        Ok(Member{
            lines: lines,
            reciever: rx,
            addr,
            state
        })
    }

    async fn send(&mut self, message: Bytes) -> Result<(), FramedInterfaceError> {
        self.lines.send(message).await
    }

    async fn broadcast(&self, message: &str) {
        let mut state = self.state.lock().await;
        let msg = format!("[{}] {}", self.addr, message);
        println!("{}", msg);
        state.broadcast(self.addr, &Bytes::from(msg));
    }

    async fn drop(&self) {
        {
            let mut state = self.state.lock().await;
            let msg = format!("[SRV] [{}] disconnected", self.addr);
            println!("{}", msg);
            state.broadcast(self.addr, &Bytes::from(msg));
        }
        self.state.lock().await.members.remove(&self.addr);
    }
}

enum Message{
    Broadcast(Bytes),
    Received(Bytes),
}

impl Stream for Member {
    type Item = Result<Message, FramedInterfaceError>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<Option<Self::Item>> {
        if let Poll::Ready(Some(v)) = Pin::new(&mut self.reciever).poll_next(cx) {
            return Poll::Ready(Some(Ok(Message::Received(v))));
        }

        let poll = Pin::new(&mut self.lines).poll_next(cx);
        match poll {
            Poll::Ready(v) => {
                match v {
                    // Something happened
                    Some(item) => {
                        match item {
                            Ok(message) => Poll::Ready(Some(Ok(Message::Broadcast(message.into())))),
                            Err(err) => Poll::Ready(Some(Err(err)))
                        }
                    },

                    // Member disconnected
                    None => Poll::Ready(None),
                }
            },
            Poll::Pending => Poll::Pending
        }
    }
}