use std::{
    error::Error,
};
use tokio::{
    net::{
        TcpStream,
    },
};

use super::super::util::{
    FramedInterface,
    create_framed_interface,
};
pub struct Client {
}

impl Client {
    pub fn new() -> Self {
        Client {

        }
    }

    pub async fn connect(addr: &str) -> Result<ClientStream, Box<dyn Error>> {
        let stream = TcpStream::connect(addr).await?;
        let lines = create_framed_interface(stream);
        
        Ok(lines)
    }
}

type ClientStream = FramedInterface<TcpStream>;

// pub enum Message {
//     Sent(Bytes),
//     Recieved(Bytes),
// }