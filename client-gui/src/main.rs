use durka_messenger_gui::ClientApp;
use iced::{Application, Settings};

fn main() {
    ClientApp::run(Settings::default());
}