use iced_futures::futures;

use durka_messenger::util::{
        Bytes,
        BytesMut,
        FramedInterface,
};
use futures::{
    Stream,
    Sink,
    SinkExt,
    StreamExt,
    stream::{
        SplitSink,
        SplitStream,
    },
};
use std::sync::Arc;
use tokio::sync::Mutex;
use tokio::net::TcpStream;

pub async fn connect(addr: &str) -> Result<Client, ()> {
    if let Ok(client) = durka_messenger::Client::connect(addr).await {
        let (w, r) = client.split();
        Ok(Client {
            addr: String::from(addr),
            // lines: Arc::new(Mutex::new(client)),
            read: Arc::new(Mutex::new(r)),
            write: Arc::new(Mutex::new(w)),
        })
    } else {
        Err(())
    }
}

#[derive(Debug, Clone)]
pub struct Client {
    addr: String,
    read: Arc<Mutex<SplitStream<FramedInterface<TcpStream>>>>,
    write: Arc<Mutex<SplitSink<FramedInterface<TcpStream>, Bytes>>>,
}

impl Client {
    pub fn get_subscription(&self) -> iced::Subscription<Output> {
        iced::Subscription::from_recipe(self.clone())
    }

    pub async fn send(&self, message: &str) -> Result<(), ()> {
        if let Ok(()) = self.write.lock().await.send(Bytes::from(format!("{}", message))).await {
            Ok(())
        } else {
            Err(())
        }
    }
}

pub type Output = Result<BytesMut, ()>;

impl<H, I> iced_native::subscription::Recipe<H, I> for Client
where H: std::hash::Hasher {
    type Output = Output;

    fn hash(&self, state: &mut H) {
        use std::hash::Hash;

        std::any::TypeId::of::<Self>().hash(state);
        self.addr.hash(state);
    }

    fn stream(
        self: Box<Self>,
        _input: futures::stream::BoxStream<'static, I>,
    ) -> futures::stream::BoxStream<'static, Self::Output> {
        Box::pin(futures::stream::unfold(
            Some(self.read),
            |state| async move {
                match state {
                    Some(read) => {
                        match read.lock().await.next().await {
                            Some(Ok(message)) => {
                                Some((Ok(message), Some(read.clone())))
                            },
                            Some(Err(_)) | None => Some((Err(()), None)),
                        }
                    },
                    None => {
                        let _: () = futures::future::pending().await;
                        
                        None
                    },
                }
            },
        ))
    }
}