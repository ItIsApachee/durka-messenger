mod client;
use client::*;

use iced::*;

use std::sync::Arc;

use durka_messenger::util::create_framed_interface;

#[derive(Debug, Clone)]
pub enum Message {
    AddressChanged(String),
    ConnectPressed,
    ConnectionResult(Result<Client, ()>),

    InputFieldChanged(String),
    Submit,
    MessageSent(Result<(), ()>),
    MessageRecieved(Output),
    // FIXME: Chat menu
}

pub enum ClientApp {
    Connect {
        address: String,
        address_state: text_input::State,
        connect: button::State,
    },
    Chat{
        client: Client,
        messages: Vec<String>,
        input: String,
        input_field: text_input::State,
        submit_button: button::State,
        scroll: scrollable::State,
    },
}

impl ClientApp {
    fn container(title: &str) -> Column<Message> {
        Column::new()
            .spacing(10)
            .push(
                Text::new(title).size(20)
            )
    }

    fn connect_menu<'a>(
        address: &'a str,
        address_state: &'a mut text_input::State,
        button_state: &'a mut button::State,
    ) -> Column<'a, Message> {
        ClientApp::container("Connect to the server")
            .padding(10)
            .push(
                TextInput::new(
                    address_state,
                    "ux.rcx.at:1337",
                    address,
                    Message::AddressChanged,
                )
                    .width(Length::Fill)
                    .padding(10)
                    .on_submit(Message::ConnectPressed)
            )
            .push(
                Row::new()
                    .push(Space::new(Length::FillPortion(3), Length::Shrink))
                    .push(Button::new(
                        button_state,
                        Text::new("Connect"),
                    )
                        .on_press(Message::ConnectPressed)
                        .width(Length::FillPortion(1)))
            )
    }

    fn chat_menu<'a>(
        messages: &'a mut Vec<String>,
        input: &'a mut String,
        input_field: &'a mut text_input::State,
        submit_button: &'a mut button::State,
        scroll: &'a mut scrollable::State,
    ) -> Column<'a, Message> {
        let mut scroll = Scrollable::new(scroll);

        for i in messages {
            scroll = scroll.push(
                Text::new(i.clone())
            )
        }

        ClientApp::container("Chat")
            .padding(10)
            .push(
                Row::new()
                    .push(
                        TextInput::new(
                            input_field,
                            "Введите свое сообщение",
                            input,
                            Message::InputFieldChanged,
                        )
                            .padding(5)
                            .width(Length::FillPortion(4))
                            .on_submit(Message::Submit)
                    )
                    .push(
                        Button::new(
                            submit_button,
                            Text::new("Отправить"),
                        )
                            .on_press(Message::Submit)
                            .width(Length::FillPortion(1))
                    )
            )
            .push(
                scroll
            )
    }
}

impl Application for ClientApp {
    type Executor = executor::Default; // Should use tokio executor
    type Message = Message;
    type Flags = ();

    fn new(_: Self::Flags) -> (Self, Command<Self::Message>) {
        (
            ClientApp::Connect {
                address: String::from(""),
                address_state: text_input::State::new(),
                connect: button::State::new(),
            },
            Command::none(),
        )
    }

    fn update(&mut self, message: Message) -> Command<Message> {
        match self {
            ClientApp::Connect{address, ..} => {
                match message {
                    Message::AddressChanged(new_address) => {
                        *address = new_address
                    },
                    Message::ConnectPressed => {
                        let addr = address.clone();
                        println!("trying to connect...");
                        return Command::perform(async move {
                            connect(&addr).await
                        }, Message::ConnectionResult);
                    },
                    Message::ConnectionResult(res) => {
                        match res {
                            Ok(client) => {
                                println!("successful connection!");
                                *self = ClientApp::Chat{
                                    client: client,
                                    messages: vec![],
                                    input: String::from(""),
                                    input_field: text_input::State::new(),
                                    submit_button: button::State::new(),
                                    scroll: scrollable::State::new(),
                                };
                            }
                            Err(_) => {
                                panic!("unimplemented error handling for connection error")
                            }
                        }
                    },
                    _ => (),
                }
            },
            ClientApp::Chat{client, messages, input, ..} => {
                match message {
                    Message::InputFieldChanged(new_input) => {
                        *input = new_input;
                    }
                    Message::Submit => {
                        let old_input = input.clone();
                        *input = "".into();
                        messages.push(format!("[YOU] {}", old_input));
                        println!("sending command to send message");
                        let client = client.clone();
                        return Command::perform(async move {
                            client.send(&old_input).await
                        }, Message::MessageSent);
                    },
                    Message::MessageSent(res) => {
                        match res {
                            Err(_) => {
                                messages.push(format!("error occured when sending message"));
                            },
                            _ => (),
                        }
                    }
                    Message::MessageRecieved(new_message) => {
                        println!("message recieved: {:?}", new_message);
                        match new_message {
                            Ok(bytes) => {
                                let new_message = std::str::from_utf8(bytes.as_ref())
                                                .map(String::from)
                                                .unwrap_or_else(|e| format!("string decoding error: {}", e));
                                messages.push(new_message);
                            },
                            Err(_) => {
                                messages.push(format!("error occured when recieving message"));
                            }
                        }
                    },
                    _ => (),
                }
            },
        }

        Command::none()
    }

    fn title(&self) -> String {
        "Durka messenger".into()
    }

    fn view(&mut self) -> Element<Message> {
        match self {
            ClientApp::Connect{address, address_state, connect} =>
                ClientApp::connect_menu(address, address_state, connect),
            ClientApp::Chat{
                messages,
                input,
                input_field,
                submit_button,
                scroll,
                ..
            } => 
                ClientApp::chat_menu(
                    messages,
                    input,
                    input_field,
                    submit_button,
                    scroll,
                ),
        }.into()
    }

    fn subscription(&self) -> Subscription<Self::Message> {
        match self {
            ClientApp::Chat{client, ..} => client.get_subscription().map(Message::MessageRecieved),
            _ => Subscription::none(),
        }
    }
}